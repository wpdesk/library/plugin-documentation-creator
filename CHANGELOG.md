## [1.2.0] - 2020-12-30
### Added
- Hook alphabetic sort
### Changed
- Rendered as a h2 not a list

## [1.1.0] - 2020-12-29
### Added
- Docker image
- FileSender

## [1.0.0] - 2020-12-28
### Added
- First version