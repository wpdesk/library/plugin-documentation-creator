# Documentation Creator

Library that helps to create a full plugin documentation.

## Usage

```php
<?php
use PluginDocumentationCreator\Creator;
use PluginDocumentationCreator\Sender\HelpScoutSender;

require_once __DIR__ . '/vendor/autoload.php';

$pluginPath = $argv[1];
$apiKey       = $argv[2] ?? $_SERVER['HELPSCOUT_API_KEY'];
$hookPageName = $argv[3] ?? $_SERVER['HELPSCOUT_HOOK_PAGE_ID'];

$hookSender = new HelpScoutSender( $apiKey, $hookPageName );
$creator = new Creator( $pluginPath );
$creator->createHooksDocumentation( $hookSender );

```