<?php

use PluginDocumentationCreator\Creator;
use PluginDocumentationCreator\Sender\FileSender;
use PluginDocumentationCreator\Sender\HelpScoutSender;

require_once __DIR__ . '/vendor/autoload.php';

$pluginPath     = $argv[1];
$apiKey         = $argv[2] ?? $_SERVER['HELPSCOUT_API_KEY'];
$hookPageName   = $argv[3] ?? $_SERVER['HELPSCOUT_HOOK_PAGE_ID'];
$hookTargetFile = $argv[4] ?? $_SERVER['HOOK_TARGET_FILE'] ?? '';

echo "--- Sending docs to HelpScout\n";
$hookSender = new HelpScoutSender( $apiKey, $hookPageName );
$creator    = new Creator( $pluginPath );
$creator->createHooksDocumentation( $hookSender );

if ( ! empty( $hookTargetFile ) ) {
	echo "--- Sending docs to File {$hookTargetFile}\n";
	$creator->createHooksDocumentation( new FileSender( $hookTargetFile ) );
}
