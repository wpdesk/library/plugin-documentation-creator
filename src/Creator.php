<?php


namespace PluginDocumentationCreator;

use HookParser\Dto\Hook;

final class Creator {
	public function __construct(
		private string $pluginSrcPath,
	) {
	}

	private function prepareHookDocs(): string {
		$parser       = new \HookParser\CodeParser();
		$renderer     = new \HookParser\HtmlRenderer();
		$fileIterator = new \HookParser\PHPFileFinder();

		$hooks = [];
		foreach ( $fileIterator->getIterator( $this->pluginSrcPath ) as $filePath ) {
			foreach ( $parser->parse( file_get_contents( current( $filePath ) ) ) as $hook ) {
				$hooks[] = $hook;
			}
		}

		usort( $hooks, function ( Hook $a, Hook $b ) {
			return strnatcmp( $a->name, $b->name );
		} );

		$output = '';
		foreach ( $hooks as $hook ) {
			$output .= $renderer->render( $hook );
		}

		return $output;
	}

	public function createHooksDocumentation( Sender\HookSender $sender ): void {
		$hookDocs = $this->prepareHookDocs();
		$sender->send( $hookDocs );
	}
}