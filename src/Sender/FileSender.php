<?php


namespace PluginDocumentationCreator\Sender;

final class FileSender implements HookSender {

	public function __construct(
		private string $filePath,
	) {
	}

	public function send( string $content ): void {
		file_put_contents( $this->filePath, $content );
	}
}