<?php


namespace PluginDocumentationCreator\Sender;

use HelpScoutDocs\ApiException;
use HelpScoutDocs\DocsApiClient;
use HelpScoutDocs\Models\Article;

final class HelpScoutSender implements HookSender {
	private Article $article;

	public function __construct(
		private string $hsApiKey,
		private string $hsHooksPage,
	) {
	}

	public function getUpdatedArticle(): ?Article {
		return $this->article;
	}

	public function send( string $content ): void {
		$api = new DocsApiClient();
		$api->setApiKey( $this->hsApiKey );

		try {
			$this->article = $api->getArticle( $this->hsHooksPage );
			$this->article->setText( $content );
			$this->article = $api->articles()->update( $this->article );
		} catch ( ApiException $e ) {
			throw new SenderException( $e->getMessage() );
		}
	}
}