<?php


namespace PluginDocumentationCreator\Sender;


interface HookSender {
	public function send(string $content): void;
}